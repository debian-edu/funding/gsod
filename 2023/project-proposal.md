# Create User Docs for Debian Edu

## About Debian Edu

> In this section, tell us about your organization or project in a few
> short paragraphs. What problem does your project solve? Who are your
> users and contributors? How long has your organization or project
> been in existence? Give some context to help us understand why
> funding your proposal would create a positive impact in open source
> and the world.

[Debian Edu](https://wiki.debian.org/DebianEdu/) (aka Skolelinux) is a
[Debian Pure Blend](https://www.debian.org/blends/) intended for use
in schools. It provides an easy installation for a main server, with
most of the services pre-configured out-of-the-box. Diskless clients
can boot from the main server via network, user authentication is
centralized, and home directories are also stored on the main
server. Thus, it simplifies a lot the installation, configuration and
maintenance of a school network. Besides, Debian Edu also maintains
packages of educational software, categorized by subject and by level,
which can be easily installed on any Debian (or Debian based) system.

*Skolelinux* and *Debian Edu* started as independent projects on 2001,
but in 2003 they joined forces together and became a standard part of
Debian. This system has been used mainly on Norwegian and German
schools, but also in other countries. Its
[docs](https://jenkins.debian.net/userContent/debian-edu-doc/) have
translations on: German, French, Italian, Spanish, Danish, Japanese,
Norwegian, Dutch, Swedish, Portuguese, Chinese, etc. This shows the
wide interest and contributions that this project had over the years.

The project objectives, since the very beginning, have been:

- **Create a complete free solution:** Provide a complete school
  solution, suitable for real-life scenarios, entirely free.
- **Reduce technical barriers:** The best way to reach a widespread
  usage is by making installation, configuration, maintenance and
  usage as easy as possible. It should work out-of-the-box.
- **International scope:** As part of a world-wide collaborative
  project, it is important to offer as many translations as possible.
- **Educational software ecosystem:** It is necessary to locate,
  package and classify educational Free Software.
- **Teaching documentation:** It is important not only to provide a
  great platform, but also to provide good documentation on how to use
  it for better teaching.


## About the GSoD project

### The problem

> Tell us about the problem your project will help solve. Why is it
> important to your organization or project to solve this problem?

The current documentation of Debian Edu is a big
[manual](https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-en/debian-edu-bookworm-manual.html)
that includes many details about the installation and
configuration. This is useful to system administrators, but not so
useful to normal non-technical users (teachers and students). There
are actually two small sections in this manual dedicated to
[students](https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-en/debian-edu-bookworm-manual.html#Users)
and
[teachers](https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-en/debian-edu-bookworm-manual.html#TeachAndLearn),
but they are almost empty. So, the documentation for non-technical
users is almost missing.

We also received feedback recently from various teachers, that the
Debian Edu manual is too complex and too technical for finding teacher
related help.

For making Debian Edu a true competitor to proprietary solutions, we
see a definite need of providing a documentation that would allow
ordinary users (teachers and students) to understand and use Debian
Edu easily. This also aligns with the main objectives of Debian Edu.

### Project’s scope

> Tell us about what documentation your organization will create,
> update, or improve. If some work is deliberately not being done,
> include that information as well. Include a time estimate, and
> whether you have already identified organization volunteers and a
> technical writer to work with your project.

We would like to create a user documentation, that explains Debian Edu
for non-technical users (teachers and students), and shows how to use
it. Some of the requirements for the new documentation are:

- It should explain things in a layman's language, trying to avoid
  complex technical terminology.
- It should be good-looking and visually attractive.
- It should be easy to edit and maintain, using the
  documentation-as-code paradigm, supporting CI/CD workflow, etc. The
  source of the docs should be maintained in a Git repository.
- It should be based on some modern documentation system, like:
  Antora, Docusaurus, MkDocs, etc.
- Besides publishing the docs on a web site, it should also be
  possible to produce nice PDF and EPUB versions of the docs.
- It should support versioning (having different versions of the docs
  for different releases of Debian Edu).
- It should support translation to different languages.

Basically it should have these main parts:
- A general introduction to Debian Edu, explaining also some of its
  basic concepts and features.
- A part that explains features available for normal users (students).
- A part that explains features available for advanced users (teachers).
- A part that explains how to do small maintenance tasks on the server
  (such as creating new user accounts, etc.).

**Optional:** If time permits, some video tutorials (screen
recordings) that explain how to use each feature would also be very
nice. As they say, a picture is worth thousands of words, and a video
tutorial is worth thousands of doc pages :smiley: .

**Optional:** If time permits, a new web site for the project should
also be created. Currently, the main homepage of the project is a wiki
page: https://wiki.debian.org/DebianEdu/. The new website should be
nice and good looking, using some static site generator tools (like
Jekyll, Hugo, etc.). The information from the wiki pages should not
just be migrated to the new homepage, but also updated with the latest
status and info about the project, and maybe reorganized as well.

**Note:** We already have a good technical writing candidate for this
project. He has participated successfully in GSoD in the past, and
also is familiar with Debian Edu. So, we are confident that he is
going to do a good job with this project. We are currently looking for
volunteers that will help with making a first review of the written
docs. We estimate that this project may take 4-5 months to complete.

### Measuring your project’s success

> How will you know that your new documentation has helped solve your
> problem? What metrics will you use, and how will you track them?

After the new documentation is finished and published, we will conduct
a survey with the users of Debian Edu, asking their opinion about how
useful they find it. If the majority of the feedback is positive, we
will consider the project successful.

We may also use a website analytics tool to find out about the usage
of the docs, which of the pages are used more frequently, etc. These
statistics might provide some additional insights.

### Timeline

> How long do you estimate this work will take? Are you able to
> breakdown the tech writer tasks by month/week?

As mentioned above, this work will take about 4-5 months to
complete. Our technical writer is already familiar with our
organization and project, so we don't need to spend time for the
introduction and orientation. However some time is needed to setup a
virtual environment for testing Debian Edu (in order to test the
instructions of the docs, etc.)

Here is a coarse estimated timeline:

| Time | Actions |
| --- | --- |
| April | Explore some documentation systems (Antora, Docusaurus, MkDocs, etc.) and decide about one that meets the requirements. Setup a virtual environment for testing Debian Edu. |
| May | Review the current docs (the manual) and the wiki pages, and decide about the structure of the new docs. Start writing the introduction part. |
| June - July | Complete the other parts of the doc. |
| August | A couple of volunteers review the docs and provide any feedback. Meanwhile the technical writer records and publishes video tutorials. |
| September | Build a new website for the Debian Edu. |

## Project budget

| Budget item | Amount [US$] | Running Total [US$] | Notes/justifications |
| --- | --- | --- | --- |
| Technical writer | 9,500.00 | 9,500.00 | |
| Volunteer stipends | 500.00 | 10,500.00 | 2 volunteer stipends x 500.00 each |
| Server costs | 500.00 | 11,000.00 | See the note below (*) |
| Total | | 11,000.00 | |

(*) An installation of a Debian Edu network is needed to develop and
test the instructions of the documentation. It will be done in a
virtual environment, which can be built with LXD in a cloud server. We
plan to get a [dedicated root
server](https://www.hetzner.com/dedicated-rootserver) on Hetzner. The
requested amount should be enough to cover the server expenses for the
duration of the documentation project.

## Additional information

> Include here any additional information that is relevant to your
> proposal.

Our technical writer has participated successfully in GSoD in the
past, so we are confident that he is going to do a good job in this
season too:
- [GSoD21: BRL-CAD](https://brlcad.org/wiki/Google_Season_of_Docs/2021/Case_Study#Results)
- [GSoD19: DVC](https://iterative.ai/blog/gsod-ideas-2020/#previous-experience)
